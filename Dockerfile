FROM ruby:2.5.1

MAINTAINER David Rodriguez <deivid.rodriguez@riseup.net>

ENV LANG C.UTF-8

RUN apt-get update \
  && apt-get install -y --no-install-recommends build-essential libpq-dev \
  && apt-get clean

RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - \
  && apt-get install -y nodejs \
  && apt-get clean

RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
  && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
  && apt-get update \
  && apt-get install -y google-chrome-stable \
  && apt-get clean

RUN CHROMEDRIVER_URL="http://chromedriver.storage.googleapis.com/2.38/chromedriver_linux64.zip" \
  && apt-get install unzip \
  && curl --silent --show-error --location --fail --retry 3 --output /tmp/chromedriver_linux64.zip $CHROMEDRIVER_URL \
  && unzip /tmp/chromedriver_linux64.zip chromedriver -d /usr/local/bin \
  && rm /tmp/chromedriver_linux64.zip

WORKDIR /app

COPY Gemfile Gemfile.lock ./
RUN bundle install

COPY package.json package-lock.json ./
RUN npm install

COPY . ./

RUN rails assets:precompile

CMD rails s
