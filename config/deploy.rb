# frozen_string_literal: true

set :application, "nipanipa"

set :repo_url, "git@gitlab.com:deivid-rodriguez/nipanipa.git"
set :branch, ENV["CI_COMMIT_REF_NAME"] || "master"

set :deploy_to, "/home/deployer/nipanipa"

set :log_level, :debug
set :pty, true

set :linked_dirs, %w[log tmp/pids tmp/cache tmp/sockets public/uploads]

set :keep_releases, 5

set :rbenv_type, :user

set :bundle_without, "deploy development test tools"

set :passenger_restart_with_touch, false

namespace :deploy do
  namespace :assets do
    desc "Install asset dependencies"
    task :install do
      on roles(:web) do
        within release_path do
          execute :npm, "install"
        end
      end
    end
  end
end

before "deploy:assets:precompile", "deploy:assets:install"
